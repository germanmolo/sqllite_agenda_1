package com.germanmolo.sqllite_agenda;

import androidx.appcompat.app.AppCompatActivity;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import database.AgendaContactos;
import database.AgendaDBHelper;
import database.Contacto;

public class MainActivity extends AppCompatActivity {

    private TextView lblRespuesta;
    private TextView lblNombre;
    private Button btnAgregar;
    private Button btnBuscar;
    private EditText txtid;
    private AgendaContactos db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        lblRespuesta = (TextView) findViewById(R.id.lblRespuesta);
        lblNombre = (TextView) findViewById(R.id.lblNombre);
        txtid = (EditText) findViewById(R.id.txtid);
        btnAgregar = (Button) findViewById(R.id.btnAgregar);
        btnBuscar = (Button) findViewById(R.id.btnBuscar);

        db = new AgendaContactos(MainActivity.this);

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Contacto c = new Contacto(1, "Jose Lopez", "6692805314", "5510686953", "Salinas 440", "Me gusta el cereal", 1);
                db.openDataBase();
                long idx = db.insertarContacto(c);
                lblRespuesta.setText("Se agrego contacto con id" + idx);

                db.closeDataBase();
            }
        });

        btnBuscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Contacto contacto;
                long id = Long.parseLong(txtid.getText().toString());
                contacto = db.getContacto(id);

                if(contacto == null) lblNombre.setText("No existe el id");
                else lblNombre.setText(contacto.getNombre());

                db.closeDataBase();
            }
        });

    }
}
